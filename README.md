# Containerization

In folder "kontejnerizacija" you will find 3 files (2 of them necessary and 1 useful) and a Django app. As containerization technology I used Docker, there are 2 containers. In first you will find simple Django app and in second one you will find Postgres database. Docker-compose is responsible for proper functioning.

*This file was written using vim. Author successfully exited it without need for a restart of his personal computer. :)*


